module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "http://localhost:3080", //這裡指定vue api要開去哪個位置
        changeOrigin: true,
        pathRewrite: {
          //這個可以把/api前綴字去除掉，後端不用特別寫上
          "^/api": "",
        },
      },
    },
  },
  chainWebpack: (config) => {
    config.plugin("html").tap((args) => {
      args[0].title = "MyApp title";
      args[0].meta = { viewport: "width=device-width,initial-scale=1,user-scalable=no" };

      return args;
    });
  },
};
