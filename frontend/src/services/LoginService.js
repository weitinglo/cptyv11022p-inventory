const axios = require("axios");

export async function userLogin(username, password) {
  //抓出特定要的值做使用
  const response = await axios.post(`/api/auth/login`, {
    username: username,
    password: password,
  });
  return response.data;
}

export async function getLogginedUser() {
  return await axios
    .get("/api/auth/loggedin_user")
    .then(res => {
      return res.data;
    })
    .catch(err => {
      return { code: 500, redirectUrl: "/Login", msg:err };
    });
}

export async function userLogout() {
  const response = await axios.post(`/api/auth/logout`);
  return response.data;
}
