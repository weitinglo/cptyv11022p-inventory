const axios = require("axios");

export async function getAllRecords() {
  return await axios
    .get(`/api/record`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getSumOfRecordqty(val) {
  return await axios
    .get(`/api/record/sumofrecordqty?val=` + val)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getSumOfNgqty(val) {
  return await axios
    .get(`/api/record/sumofngqty?val=` + val)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getSumOfUsedqty(val) {
  return await axios
    .get(`/api/record/sumofusedqty?val=` + val)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getRecordsOnKeywords(page, keywords) {
  return await axios
    .get(`/api/record/keywords?page=` + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getRecordsOnPage(page) {
  return await axios
    .get(`/api/record/on_page?page=` + page)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCountOnKeywords(keywords) {
  return await axios
    .get(`/api/record/keywords_total_count?keywords=` + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCount() {
  return await axios
    .get(`/api/record/total_count`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function addRecord(data) {
  return await axios
    .put(`/api/record`, { record: data })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    }); //之後data這包可以叫做record
}

export async function deleteRecord(id, cid, mid, quantity, categoryName, name) {
  return await axios
    .delete(`/api/record?id=` + id + "&cid=" + cid + "&mid=" + mid + "&quantity=" + quantity + "&categoryName=" + categoryName + "&name=" + name)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function editRecord(record) {
  return await axios
    .post(`/api/record`, { record: record })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}
