const axios = require("axios");

export async function getAllExchanges() {
  return await axios
    .get(`/api/exchange`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getExchangesOnKeywords(page, keywords) {
  return await axios
    .get(`/api/exchange/keywords?page=` + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getExchangeSumOnKeywords(page, keywords) {
  return await axios
    .get(`/api/exchange/sum/keywords?page=` + page + "&keywords=" + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getExchangesOnPage(page) {
  return await axios
    .get(`/api/exchange/on_page?page=` + page)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCountOnKeywords(keywords) {
  return await axios
    .get(`/api/exchange/keywords_total_count?keywords=` + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalSumDataCountOnKeywords(keywords) {
  return await axios
    .get(`/api/exchange/sum/keywords_total_count?keywords=` + keywords)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function getTotalDataCount() {
  return await axios
    .get(`/api/exchange/total_count`)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function addExchange(exchange) {
  return await axios
    .put(`/api/exchange`, { exchangeData: exchange })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    }); //之後data這包可以叫做record
}

export async function deleteExchange(id, category, name, ngqty, changedqty, otw) {
  return await axios
    .delete(`/api/exchange?id=` + id + "&category=" + category + "&name=" + name + "&ngqty=" + ngqty + "&changedqty=" + changedqty + "&otw=" + otw)
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}

export async function editExchange(exchange) {
  return await axios
    .post(`/api/exchange`, { exchangeData: exchange })
    .then((res) => {
      return res.data;
    })
    .catch((err) => {
      return { code: 500, redirectUrl: "/Login", msg: err };
    });
}
