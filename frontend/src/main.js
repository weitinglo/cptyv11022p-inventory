import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import VueSweetAlert from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";
import JQuery from "jquery";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import Paginate from "vuejs-paginate-next";
import "@fortawesome/fontawesome-free/css/all.css";
import { vfmPlugin } from "vue-final-modal";
import VueSidebarMenu from "vue-sidebar-menu";
import "vue-sidebar-menu/dist/vue-sidebar-menu.css";

import "./assets/styles/global.css";

let app = createApp(App);
app.use(router);
app.use(VueSweetAlert);
// app.component('font-awesome-icon', FontAwesomeIcon)
app.use(JQuery);
app.use(Paginate);
app.use(vfmPlugin);
app.use(VueSidebarMenu);

app.mount("#app");
