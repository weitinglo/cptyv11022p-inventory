import { createRouter, createWebHistory } from "vue-router";
import Dashboard from "../views/Dashboard.vue";
import Record from "../views/Record.vue";
import Users from "../views/Users.vue";
import Material from "../views/Material.vue";
import Log from "../views/Log.vue";
import Login from "../views/Login.vue";
import Profile from "../views/Profile.vue";
import Exchange from "../views/Exchange.vue";
import ExchangeSum from "../views/ExchangeSum.vue";

const routes = [
  {
    path: "/login",
    name: "Login",
    component: Login,
  },
  {
    path: "/",
    component: Dashboard,
    children: [
      {
        path: "/",
        component: Record,
      },
      {
        path: "users",
        component: Users,
      },
      {
        path: "log",
        component: Log,
      },
      {
        path: "material",
        component: Material,
      },
      {
        path: "profile",
        component: Profile,
      },
      {
        path: "Record",
        component: Record,
      },
      {
        path: "Exchange",
        component: Exchange,
      },
      {
        path: "ExchangeSum",
        component: ExchangeSum,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});
router.beforeEach(function(to, from, next) {
  //Navigation Guards, 每一個路由要進入前，都要先經過這裡
  next();
  // console.log(to.name)
  // if (to.name !== "Login")
  //   next({ name: "Login" });
  // else
  //   next();
});
export default router;
