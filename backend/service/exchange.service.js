const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");

/*Get all exchange */
const getAllExchanges = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT * FROM EXCHANGE`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get records on page*/
const getExchangesOnPage = (page) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT 
      EXCHANGE.ID AS id, 
      CATEGORY, 
      EXCHANGE.NAME AS name, 
      EXCHANGE.USER AS user, 
      USER.NAME AS username, 
      NGQTY, 
      CHANGEDQTY, 
      NOTES,
      DATETIME 
    FROM 
      EXCHANGE 
    LEFT JOIN USER ON 
      EXCHANGE.USER = USER.ID 
    ORDER BY 
      ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, [offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get records on key words and page*/
const getExchangesOnKeywords = (page, keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT 
      EXCHANGE.ID AS id, 
      CATEGORY.HARDWARE AS category,
      MATERIAL.NAME AS name, 
      CATEGORY.ID AS cid,
      MATERIAL.ID AS mid,
      EXCHANGE.USER AS user,
      USER.NAME AS username, 
      NGQTY, 
      CHANGEDQTY,
      OTW,
      EXCHANGE.NOTES, 
      EXCHANGE.DATETIME 
    FROM 
      EXCHANGE
    LEFT JOIN USER ON 
      EXCHANGE.USER = USER.ID
    LEFT JOIN MATERIAL ON 
      EXCHANGE.NAME = MATERIAL.ID
    LEFT JOIN CATEGORY ON 
      EXCHANGE.CATEGORY = CATEGORY.ID
    WHERE 
      (MATERIAL.NAME LIKE ? OR USER.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ?)
    ORDER BY 
      ID DESC LIMIT 10 OFFSET ?
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get records on key words and page*/
const getExchangeSumOnKeywords = (page, keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;

    let sqlQuery = `
      SELECT 
        EXCHANGE.ID AS id,
        CATEGORY.HARDWARE AS category,
        MATERIAL.NAME AS name,
        EXCHANGE.USER AS user,
        USER.NAME AS username,
        MATERIAL.TOTAL AS total,
        SUM(EXCHANGE.NGQTY) AS sumofngqty, 
        SUM(EXCHANGE.CHANGEDQTY) AS sumofchangedqty, 
        SUM(EXCHANGE.OTW) AS sumofotwqty, 
        EXCHANGE.NOTES,
        EXCHANGE.DATETIME
      FROM
        EXCHANGE
      LEFT JOIN USER ON
        EXCHANGE.USER = USER.ID
      LEFT JOIN MATERIAL ON
        EXCHANGE.NAME = MATERIAL.ID
      LEFT JOIN CATEGORY ON 
        EXCHANGE.CATEGORY = CATEGORY.ID
      WHERE
        (MATERIAL.NAME LIKE ? OR USER.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ?)
      GROUP BY 
        MATERIAL.NAME
      ORDER BY
        ID DESC LIMIT 10 OFFSET ?
      `;

    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
        // console.log(err);
      }
    });
  });
};

/*Get total record count on keywords */
const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT
      COUNT(*) AS total
    FROM
      EXCHANGE
    LEFT JOIN
      USER ON EXCHANGE.USER = USER.ID
    LEFT JOIN
      CATEGORY ON EXCHANGE.CATEGORY = CATEGORY.ID  
    LEFT JOIN
      MATERIAL ON EXCHANGE.NAME = MATERIAL.ID
    WHERE
      (MATERIAL.NAME LIKE ? OR USER.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ?)
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%"], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get total record count on keywords */
const getTotalSumDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT
        *
      FROM
        EXCHANGE
      LEFT JOIN USER ON
        EXCHANGE.USER = USER.ID
      LEFT JOIN MATERIAL ON
        EXCHANGE.NAME = MATERIAL.ID
      LEFT JOIN CATEGORY ON
        EXCHANGE.CATEGORY = CATEGORY.ID
      WHERE
        (MATERIAL.NAME LIKE ? OR USER.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ?)
      GROUP BY
        EXCHANGE.NAME
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%"], (err, rows) => {
      if (!err) {
        jsonRes.data = rows.length;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get total record count */
const getTotalDataCount = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) AS total FROM EXCHANGE`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const addExchange = (exchangeData) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    var today = new Date();
    var date = today.getFullYear() + "-" + String(today.getMonth() + 1).padStart(2, "0") + "-" + String(today.getDate()).padStart(2, "0");
    var time = String(today.getHours()).padStart(2, "0") + ":" + String(today.getMinutes()).padStart(2, "0") + ":" + String(today.getSeconds()).padStart(2, "0");
    var dateTime = date + " " + time;

    let sqlQuery = `
    INSERT INTO 
      EXCHANGE(
      CATEGORY,
      NAME,
      USER,
      NGQTY,
      CHANGEDQTY,
      OTW,
      NOTES,
      DATETIME
      ) VALUES(?,?,?,?,?,?,?,?)`;
    db.all(sqlQuery, [exchangeData.category, exchangeData.name, exchangeData.user, exchangeData.ngqty, exchangeData.changedqty, exchangeData.otw, exchangeData.notes, dateTime], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const deleteExchange = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `DELETE FROM EXCHANGE WHERE ID = ? ;`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const editExchange = (exchangeData) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `
    UPDATE 
      EXCHANGE 
    SET 
      NAME=?, 
      USER=?, 
      NGQTY=?, 
      CHANGEDQTY=?, 
      OTW=?,
      NOTES=? 
    WHERE 
      ID = ?;`;
    db.all(sqlQuery, [exchangeData.name, exchangeData.user, exchangeData.ngqty, exchangeData.changedqty, exchangeData.otw, exchangeData.notes, exchangeData.id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  getAllExchanges,
  getExchangesOnPage,
  getExchangesOnKeywords,
  getExchangeSumOnKeywords,
  getTotalDataCountOnKeywords,
  getTotalSumDataCountOnKeywords,
  getTotalDataCount,
  addExchange,
  deleteExchange,
  editExchange,
};
