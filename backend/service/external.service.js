const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("inventory-db.db");

const postExternal = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    UPDATE
      USED
    SET
      SUM = SUM + STANDARD
    WHERE
      SID = ?;
    `;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  postExternal,
};
