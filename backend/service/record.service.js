const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");

/*Get all record */
const getAllRecords = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT
        *
      FROM
        RECORD
    `;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get sumofrecordqty */
const getSumOfRecordqty = (val) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT
        SUM(RECORD.QUANTITY) AS sumofrecordqty
      FROM
        RECORD
      WHERE
        RECORD.NAME = ?
      `;
    db.all(sqlQuery, [val], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get sumofngqty */
const getSumOfNgqty = (val) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT 
        SUM(EXCHANGE.NGQTY) AS sumofngqty
      FROM
        EXCHANGE
      WHERE
        EXCHANGE.NAME = ?
      `;
    db.all(sqlQuery, [val], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get sumofusedqty */
const getSumOfUsedqty = (val) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT 
        SUM(USED.SUM) AS sumofusedqty
      FROM
        USED
      WHERE
        USED.MID = ?
    `;
    db.all(sqlQuery, [val], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get records on page*/
const getRecordsOnPage = (page) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT
      *
    FROM
      RECORD
    ORDER BY
      ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, [offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get records on key words and page*/
const getRecordsOnKeywords = (page, keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT 
      RECORD.ID AS id, 
      CATEGORY.HARDWARE AS category,
      CATEGORY.ID AS cid,
      MATERIAL.ID AS mid,
      MATERIAL.NAME AS name,
      RECORD.USER AS user, 
      USER.NAME AS username, 
      RECORD.QUANTITY,
      RECORD.NOTES,
      DATETIME 
    FROM 
      RECORD
    LEFT JOIN MATERIAL ON
      RECORD.NAME = MATERIAL.ID
    LEFT JOIN CATEGORY ON
      RECORD.CATEGORY = CATEGORY.ID
    LEFT JOIN USER ON 
      RECORD.USER = USER.ID
    WHERE 
      (MATERIAL.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ? OR USER.NAME LIKE ?) 
    ORDER BY 
      ID DESC LIMIT 10 OFFSET ?
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get total record count on keywords */
const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT 
      COUNT(*) AS total 
    FROM 
      RECORD
    LEFT JOIN USER ON 
      RECORD.USER = USER.ID
    LEFT JOIN CATEGORY ON 
      RECORD.CATEGORY = CATEGORY.ID
    LEFT JOIN MATERIAL ON 
      RECORD.NAME = MATERIAL.ID
    WHERE 
      (MATERIAL.NAME LIKE ? OR USER.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ?)
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%"], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

/*Get total record count */
const getTotalDataCount = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) AS total FROM RECORD`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const addRecord = (record) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    var today = new Date();
    var date = today.getFullYear() + "-" + String(today.getMonth() + 1).padStart(2, "0") + "-" + String(today.getDate()).padStart(2, "0");
    var time = String(today.getHours()).padStart(2, "0") + ":" + String(today.getMinutes()).padStart(2, "0") + ":" + String(today.getSeconds()).padStart(2, "0");
    var dateTime = date + " " + time;

    let sqlQuery = `INSERT INTO RECORD(CATEGORY,NAME,USER,QUANTITY,NOTES,DATETIME) VALUES(?,?,?,?,?,?);`;
    db.all(sqlQuery, [record.cid, record.mid, record.user, record.quantity, record.notes, dateTime], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const deleteRecord = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `DELETE FROM RECORD WHERE ID = ? ;`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const editRecord = (record) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `
    UPDATE 
      RECORD 
    SET 
      CATEGORY=?, NAME=?, USER=?, QUANTITY=?, NOTES=? 
    WHERE 
      ID = ?;
    `;
    db.all(sqlQuery, [record.cid, record.mid, record.user, record.quantity, record.notes, record.id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  getAllRecords,
  getSumOfRecordqty,
  getSumOfNgqty,
  getSumOfUsedqty,
  getRecordsOnPage,
  getRecordsOnKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  addRecord,
  deleteRecord,
  editRecord,
};
