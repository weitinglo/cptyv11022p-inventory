const sqlite3 = require("sqlite3").verbose();
let db = new sqlite3.Database("inventory-db.db");
var bcrypt = require("bcrypt");

const getAllUsers = () => {
  return new Promise((resolve, reject) => {
    //3
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
        SELECT * FROM USER`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        //4
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getUserById = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
        SELECT * FROM USER WHERE ID = ?`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0];
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getUsersOnPage = (page) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
      SELECT * FROM USER ORDER BY ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, [offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        //4
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};


const getUsersOnPageKeywords = (page,keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
      SELECT ID, USERNAME, NAME, LEVEL, EMAIL, PHONE FROM USER WHERE (NAME LIKE ? OR USERNAME LIKE ?) ORDER BY ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, ['%' + keywords + '%','%' + keywords + '%',offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        //4
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT COUNT(*) AS total FROM USER WHERE (NAME LIKE ? OR USERNAME LIKE ?)`;
    db.all(sqlQuery, ['%' + keywords + '%','%' + keywords + '%'], (err, rows) => {
        if (!err) {
        jsonRes.data = rows[0].total;
        //4
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};


const getTotalDataCount = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT COUNT(*) AS total FROM USER`;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        //4
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const addUser = (user) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    bcrypt.hash(user.password, 10, function (err, hash) {
      let sqlQuery = `INSERT INTO USER(USERNAME,NAME,LEVEL,PASSWORD) VALUES(?,?,?,?);`;
      db.all(
        sqlQuery,
        [user.username, user.name, user.level, hash],
        (err, rows) => {
          if (!err) {
            resolve(jsonRes);
          } else {
            jsonRes.msg = "failed";
            reject(jsonRes);
          }
        }
      );
    });
  });
};

const deleteUser = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `DELETE FROM USER WHERE ID = ? ;`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const editUser = (user) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `UPDATE USER SET USERNAME=?, NAME=?,  LEVEL=? WHERE ID = ?;`;
    db.all(
      sqlQuery,
      [user.username, user.name, user.level, user.id],
      (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
};

const resetUserPassword = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    bcrypt.hash('chiper9101', 10, function (err, hash) {
      let sqlQuery = `UPDATE USER SET PASSWORD=? WHERE ID = ?;`;
      db.all(sqlQuery, [hash,id], (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      });
    });
    
  });
};

const getUserProfile = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
      SELECT ID, USERNAME, NAME, LEVEL, EMAIL, PHONE FROM USER WHERE ID = ?`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const updateProfile = (id, profile) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    
    let sqlQuery = `UPDATE USER SET NAME=?, EMAIL = ?, PHONE = ? WHERE ID = ?;`;
    db.all(
      sqlQuery,
      [profile.name, profile.email, profile.phone, id],
      (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      }
    );
  });
};

const updatePassword = (id, password) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    bcrypt.hash(password.new, 10, function (err, hash) {
      let sqlQuery = `UPDATE USER SET PASSWORD=? WHERE ID = ?;`;
      db.all(sqlQuery, [hash, id], (err, rows) => {
        if (!err) {
          resolve(jsonRes);
        } else {
          jsonRes.msg = "failed";
          reject(jsonRes);
        }
      });
    });

  });
};

module.exports = {
  getAllUsers,
  getUserById,
  getUsersOnPage,
  getUsersOnPageKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  addUser,
  deleteUser,
  editUser,
  resetUserPassword,
  getUserProfile,
  updateProfile,
  updatePassword,
};
