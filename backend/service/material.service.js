const sqlite3 = require("sqlite3").verbose();

let db = new sqlite3.Database("inventory-db.db");

const getAllMaterials = () => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT
      MATERIAL.ID AS id,
      CATEGORY.HARDWARE AS category,
      MATERIAL.ID AS mid,
      CATEGORY.ID AS cid,
      MATERIAL.NAME AS name,
      CODE,
      MATERIAL.QUANTITY,
      TOTAL,
      UNIT,
      MATERIAL.NOTES
    FROM
      MATERIAL
    LEFT JOIN CATEGORY ON
      MATERIAL.CATEGORY = CATEGORY.ID
    `;
    db.all(sqlQuery, (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getMaterialOnPage = (page) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT
      *
    FROM
      MATERIAL
    ORDER BY
      ID DESC LIMIT 10 OFFSET ?`;
    db.all(sqlQuery, [offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const checkExistingCode = (code) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT COUNT(*) AS count FROM MATERIAL WHERE CODE = ?`;
    db.all(sqlQuery, [code], (err, rows) => {
      if (!err) {
        resolve(rows[0].count);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getMaterialOnPageOnKeywords = (page, keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let offset = (page - 1) * 10;
    let sqlQuery = `
    SELECT
      MATERIAL.ID AS id,
      CATEGORY.HARDWARE AS category,
      CATEGORY.ID AS cid,
      MATERIAL.NAME AS name,
      CODE,
      QUANTITY,
      TOTAL,
      UNIT,
      NOTES
    FROM
      MATERIAL
    LEFT JOIN CATEGORY ON
      MATERIAL.CATEGORY = CATEGORY.ID
    WHERE 
      (MATERIAL.NAME LIKE ? OR CATEGORY.HARDWARE LIKE ? OR MATERIAL.CODE LIKE ?) 
    ORDER BY
      CATEGORY DESC LIMIT 10 OFFSET ?
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%", offset], (err, rows) => {
      if (!err) {
        jsonRes.data = rows;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const addMaterial = (material) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `INSERT INTO MATERIAL(CATEGORY,NAME,CODE,QUANTITY,TOTAL,UNIT,NOTES) VALUES(?,?,?,?,?,?,?);`;
    db.all(sqlQuery, [material.category, material.name, material.code, material.quantity, material.total, material.unit, material.notes], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const deleteMaterial = (id) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `DELETE FROM MATERIAL WHERE ID = ? ;`;
    db.all(sqlQuery, [id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const editMaterial = (material) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `
    UPDATE 
      MATERIAL 
    SET 
      CATEGORY=?, 
      NAME=?, 
      CODE=?, 
      QUANTITY=?, 
      TOTAL=?,
      UNIT=?, 
      NOTES=? 
    WHERE 
      ID = ?
    ;`;
    db.all(sqlQuery, [material.category, material.name, material.code, material.quantity, material.total, material.unit, material.notes, material.id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const substractMaterialQuantity = (quantity, cid, mid) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `
    UPDATE 
      MATERIAL
    SET 
      QUANTITY = QUANTITY - ?
    WHERE 
      CATEGORY = ? AND ID = ?
    ;`;
    db.all(sqlQuery, [quantity, cid, mid], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const substractMaterialQuantityById = (id, number) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };

    let sqlQuery = `UPDATE MATERIAL SET QUANTITY=QUANTITY-? WHERE ID = ?;`;
    db.all(sqlQuery, [number, id], (err, rows) => {
      if (!err) {
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

const getTotalDataCountOnKeywords = (keywords) => {
  return new Promise((resolve, reject) => {
    var jsonRes = {
      code: 200,
      msg: "success",
      data: "",
    };
    let sqlQuery = `
    SELECT 
      COUNT(*) as total 
    FROM 
      MATERIAL
    LEFT JOIN CATEGORY ON
      MATERIAL.CATEGORY = CATEGORY.ID
    WHERE 
      (CATEGORY.HARDWARE LIKE ? OR NAME LIKE ? OR CODE LIKE ?)
    `;
    db.all(sqlQuery, ["%" + keywords + "%", "%" + keywords + "%", "%" + keywords + "%"], (err, rows) => {
      if (!err) {
        jsonRes.data = rows[0].total;
        resolve(jsonRes);
      } else {
        jsonRes.msg = "failed";
        reject(jsonRes);
      }
    });
  });
};

module.exports = {
  getAllMaterials,
  getMaterialOnPage,
  getMaterialOnPageOnKeywords,
  checkExistingCode,
  addMaterial,
  deleteMaterial,
  editMaterial,
  substractMaterialQuantity,
  substractMaterialQuantityById,
  getTotalDataCountOnKeywords,
};
