const router = require("express").Router();
let passport = require("../middleware/auth");

const { addExchange, getAllExchanges, getExchangesOnKeywords, getExchangeSumOnKeywords, getTotalDataCountOnKeywords, getTotalSumDataCountOnKeywords, deleteExchange, editExchange, getExchangesOnPage, getTotalDataCount } = require("../controller/exchange.controller");

const authenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  }
  return res.redirect("/Login");
};

/*Get all exchange*/
router.get("/", async (req, res) => {
  try {
    const results = await getAllExchanges();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get records on page*/
router.get("/on_page", authenticated, async (req, res) => {
  try {
    const results = await getExchangesOnPage(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get records on keywords and page*/
router.get("/keywords", authenticated, async (req, res) => {
  try {
    const results = await getExchangesOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get records on keywords and page*/
router.get("/sum/keywords", authenticated, async (req, res) => {
  try {
    const results = await getExchangeSumOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get total record count on keywords*/
router.get("/keywords_total_count", authenticated, async (req, res) => {
  try {
    const results = await getTotalDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get total record count on keywords*/
router.get("/sum/keywords_total_count", authenticated, async (req, res) => {
  try {
    const results = await getTotalSumDataCountOnKeywords(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Get total record count*/
router.get("/total_count", async (req, res) => {
  try {
    const results = await getTotalDataCount();
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Add a record*/
router.put("/", async (req, res) => {
  try {
    const results = await addExchange(req); //給到controller
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Delete a record*/
router.delete("/", async (req, res) => {
  try {
    const results = await deleteExchange(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

/*Edit a record*/
router.post("/", async (req, res) => {
  try {
    const results = await editExchange(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
