const router = require("express").Router();
const { postExternal } = require("../controller/external.controller");

router.post("/:id", async (req, res) => {
  try {
    const results = await postExternal(req);
    res.send(results);
  } catch (error) {
    res.status(500).send(error);
  }
});

module.exports = router;
