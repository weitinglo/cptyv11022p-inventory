const userService = require("../service/user.service");
const logService = require("../service/log.service");
var bcrypt = require("bcrypt");

const getAllUsers = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      //2
      const res = await userService.getAllUsers();
      //5
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getUsersOnPage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      const res = await userService.getUsersOnPage(page);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getUsersOnPageKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;

      const res = await userService.getUsersOnPageKeywords(page, keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await userService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCount = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await userService.getTotalDataCount();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const addUser = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const user = req.body.user;
      const res = await userService.addUser(user);
      logService.addLog(req, "新增使用者", " 名稱-" + user.name);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const deleteUser = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.query.id;
      const res = await userService.deleteUser(id);
      logService.addLog(req, "刪除使用者", " id-" + id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const editUser = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const user = req.body.user;
      const res = await userService.editUser(user);
      logService.addLog(req, "更新使用者", "原本資料:名稱-" + user.name + " 使用者-" + user.username + " 層級-" + user.level);

      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const resetUserPassword = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.body.id;
      const res = await userService.resetUserPassword(id);
      logService.addLog(req, "預設密碼", " id-" + id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getUserProfile = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let id = req.session.passport.user.id;
      const res = await userService.getUserProfile(id);

      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const updateProfile = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let id = req.session.passport.user.id;
      const profile = req.body.profile;
      const res = await userService.updateProfile(id, profile);
      logService.addLog(req, "更新個人頁面帳號", " id-" + id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const updatePassword = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let id = req.session.passport.user.id;
      const password = req.body.password;
      const user = await userService.getUserById(id);
      response = {};
      if (bcrypt.compareSync(password.current, user.data.password)) {
        response = await userService.updatePassword(id, password);
        logService.addLog(req, "更新個人頁面密碼", "id-" + id);
      } else {
        response.code = 401;
        response.msg = "目前密碼輸入錯誤";
      }
      resolve(response);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getAllUsers,
  getUsersOnPage,
  getUsersOnPageKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  getUserProfile,
  addUser,
  deleteUser,
  editUser,
  resetUserPassword,
  updateProfile,
  updatePassword,
};
