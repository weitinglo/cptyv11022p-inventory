const logService = require("../service/log.service");

const getAllLogs = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await logService.getAllLogs();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getLogsOnPageOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await logService.getLogsOnPageOnKeywords(page,keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const addLog = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const log = req.body.log;
      const res = await logService.addLog(log);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await logService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getAllLogs,
  getLogsOnPageOnKeywords,
  addLog,
  getTotalDataCountOnKeywords,
};
