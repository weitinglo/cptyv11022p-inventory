const recordService = require("../service/record.service");
const logService = require("../service/log.service");
const materialService = require("../service/material.service");

const getAllRecords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await recordService.getAllRecords();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getSumOfRecordqty = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let val = req.query.val;
      const res = await recordService.getSumOfRecordqty(val);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getSumOfNgqty = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let val = req.query.val;
      const res = await recordService.getSumOfNgqty(val);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getSumOfUsedqty = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let val = req.query.val;
      const res = await recordService.getSumOfUsedqty(val);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getRecordsOnPage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      const res = await recordService.getRecordsOnPage(page);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getRecordsOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await recordService.getRecordsOnKeywords(page, keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await recordService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCount = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await recordService.getTotalDataCount();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const addRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const record = req.body.record; //record : data會跑到這
      const res = await recordService.addRecord(record); //functiont傳值到下一層
      materialService.substractMaterialQuantity(record.quantity, record.cid, record.mid); //傳去減掉庫存的量
      logService.addLog(req, "新增領料", " 類別-" + record.categoryNameInString + " 名稱-" + record.materialNameInString + " 數量-" + record.quantity); //記錄到歷史紀錄裡
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const deleteRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.query.id;
      const cid = req.query.cid;
      const mid = req.query.mid;
      const quantity = req.query.quantity;
      const categoryName = req.query.categoryName;
      const name = req.query.name;

      materialService.substractMaterialQuantity(-quantity, cid, mid); //加回庫存的量，負負得正
      const res = await recordService.deleteRecord(id);
      logService.addLog(req, "刪除領料", " 類別-" + categoryName + " 名稱-" + name + " 數量-" + quantity);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const editRecord = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const record = req.body.record;
      const res = await recordService.editRecord(record);
      materialService.substractMaterialQuantity(record.quantity - record.originalQty, record.cid, record.mid); //減掉庫存的量
      logService.addLog(req, "更新領料", "更新資料:類別-" + record.categoryNameInString + " 名稱-" + record.materialNameInString + " 數量-" + record.quantity);

      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getAllRecords,
  getSumOfRecordqty,
  getSumOfNgqty,
  getSumOfUsedqty,
  getRecordsOnPage,
  getRecordsOnKeywords,
  getTotalDataCountOnKeywords,
  getTotalDataCount,
  addRecord,
  deleteRecord,
  editRecord,
};
