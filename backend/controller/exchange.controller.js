const exchangeService = require("../service/exchange.service");
const logService = require("../service/log.service");
const materialService = require("../service/material.service");

const getAllExchanges = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await exchangeService.getAllExchanges();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getExchangesOnPage = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      const res = await exchangeService.getExchangesOnPage(page);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getExchangesOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await exchangeService.getExchangesOnKeywords(page, keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getExchangeSumOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let page = req.query.page;
      let keywords = req.query.keywords;
      const res = await exchangeService.getExchangeSumOnKeywords(page, keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await exchangeService.getTotalDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalSumDataCountOnKeywords = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      let keywords = req.query.keywords;
      const res = await exchangeService.getTotalSumDataCountOnKeywords(keywords);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const getTotalDataCount = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await exchangeService.getTotalDataCount();
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const addExchange = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const exchangeData = req.body.exchangeData; //record : data會跑到這
      const res = await exchangeService.addExchange(exchangeData); //functiont傳值到下一層
      // materialService.substractMaterialQuantity(record.category, record.name, record.quantity); //減掉庫存的量
      logService.addLog(req, "新增料件交換", " 名稱-" + exchangeData.materialNameInString + " ng數量-" + exchangeData.ngqty + " 已換數量-" + exchangeData.changedqty + " 送中科院-" + exchangeData.otw); //記錄到歷史紀錄裡
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const deleteExchange = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.query.id;
      const category = req.query.category;
      const name = req.query.name;
      const ngqty = req.query.ngqty;
      const changedqty = req.query.changedqty;
      const otw = req.query.otw;

      materialService.substractMaterialQuantity(category, name); //減掉庫存的量
      const res = await exchangeService.deleteExchange(id);
      logService.addLog(req, "刪除料件交換", " 類別-" + category + " 名稱-" + name + " ng數量-" + ngqty + " 已換數量-" + changedqty + " 送中科院-" + otw);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

const editExchange = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const exchangeData = req.body.exchangeData;
      const res = await exchangeService.editExchange(exchangeData);
      // materialService.substractMaterialQuantity(exchangeData.category, exchangeData.name, exchangeData.quantity - exchangeData.originalQty); //減掉庫存的量
      logService.addLog(req, "更新料件交換", "更新資料:名稱-" + exchangeData.materialNameInString + " ng數量-" + exchangeData.ngqty + " 已換數量-" + exchangeData.changedqty + " 送中科院-" + exchangeData.otw);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  getAllExchanges,
  getExchangesOnPage,
  getExchangesOnKeywords,
  getExchangeSumOnKeywords,
  getTotalDataCountOnKeywords,
  getTotalSumDataCountOnKeywords,
  getTotalDataCount,
  addExchange,
  deleteExchange,
  editExchange,
};
