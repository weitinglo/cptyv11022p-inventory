const externalService = require("../service/external.service");

const postExternal = async (req, res) => {
  return new Promise(async (resolve, reject) => {
    try {
      const id = req.params.id;
      const res = await externalService.postExternal(id);
      resolve(res);
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  postExternal,
};
